
function cv
	set -l config_dir ~/.pysv
	set -l options --global

	if [ "$PYSV_PARSE_CONF" = 1 ]
		set -l sh_conf
		if [ -f $config_dir/cv.conf ]
			set sh_conf $config_dir/cv.conf
		else
			set sh_conf /usr/local/lib/pysv/default_config/cv.conf
		end
		set options $options --sh-conf $sh_conf
	end

	sv $options $argv
end

function cva
	if [ $argv[1] = "-h" ]
		echo "Usage: cva <KEY> [PATH]"
		echo "Raccourci pour cv -A: ajoute le raccourci global <KEY> pour le répertoire"
		echo "courant, ou pour [PATH] si précisé."
		return
	end

	cv -A $argv || return
	cv # TODO: à améliorer (double exécution)
end
