
# Création des commandes:
source /usr/local/lib/pysv/shell_wrappers/fish/sv.fish   # Commandes fish 'sv' et 'sva'
source /usr/local/lib/pysv/shell_wrappers/fish/cv.fish   # Commandes fish 'cv' et 'cva'

# Facultatif: Autoriser la lecture des fichiers de configuration, et donc la
#             création automatique d'alias et de variables pour chaque raccourci:
if test (count $argv) -gt 0; and test $argv[1] = --full
	set PYSV_PARSE_CONF 1
	cv >/dev/null 2>&1   # Crée les alias et variables de raccourcis globaux dès le chargement
end
