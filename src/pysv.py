#!/usr/bin/python3
# coding: utf-8

import sys
import os
from pathlib import Path
import argparse

from pysvlib import *

#-------------------- def create_parser(short_help=false)
def create_parser(short_help=False):
    parser = argparse.ArgumentParser(
        prog = 'pysv',
        description = "Gère les fichiers '.svinfo' .",
        add_help = False,
    )


    # NON-OPTION ARGUMENT:

    parser.add_argument(
        'keyword',
        metavar='KEY',
        nargs='?',
        default='',
        help="Raccourci désignant un chemin soit local à un un projet, soit dépendant de l'option -L")


    # OPTIONS FOR SETTING CONTEXT:

    parser.add_argument(
        '-C',
        '--directory',
        metavar='PATH',
        help="Exécute pysv comme si PATH était le répertoire courant")

    parser.add_argument(
        '-L',
        '--svinfo-path',
        metavar='PATH',
        help="Choisit un fichier svinfo particulier au lieu de chercher dans l'arborescence courante")

    parser.add_argument(
        '-g',
        '--global',
        action='store_true',
        dest='is_global',
        help="Choisit le fichier $HOME/.svinfo.global comme fichier svinfo "+
             "(équivalent de \"-L ~/.svinfo.global\"). Si la variable d'environnement "+
             "PYSV_GLOBAL_SVINFO existe, utilise cette variable à la place.")


    # OPTIONS FOR GETTING INFORMATION:

    parser.add_argument(
        '-i',
        '--get-svinfo-file',
        action='store_true',
        help="Affiche le chemin vers le fichier .svinfo trouvé (ou précisé, cf option -L)")

    parser.add_argument(
        '-l',
        '--list',
        nargs='*',
        metavar='KEY',
        help="Affiche les raccourcis actuels (contenu du fichier .svinfo trouvé). "+
             "Avec l'option '-u', affiche les chemins complets vers ces raccourcis.")

    parser.add_argument(
        '--get-keys',
        action='store_true',
        help="Affiche la liste de tous les noms de raccourci existants")

    parser.add_argument(
        '--check',
        action='store_true',
        help="Vérifie l'existence des chemins de chaque raccourci et affiche les problèmes trouvés")

    parser.add_argument(
        '-p',
        '--get-project-dir',
        action='store_true',
        help="Affiche le chemin racine où se trouve le fichier .svinfo trouvé (ou précisé, cf option -L)")

    parser.add_argument(
        '-s',
        '--search',
        nargs='?',
        metavar='PATH',
        const='.',
        help="Cherche le répertoire courant (ou le répertoire indiqué PATH) dans les raccourcis")



    # OPTIONS FOR MODIFYING THINGS:

    parser.add_argument(
        '-a',
        '--add',
        nargs='+',
        metavar='KEY',
        help="Ajoute un mot-clé pour le répertoire courant")

    parser.add_argument(
        '-A',
        '--add-absolute',
        nargs='+',
        metavar='KEY',
        help="Ajoute un mot-clé pour le répertoire courant, avec un chemin absolu (équivalent de -ua)")

    parser.add_argument(
        '-r',
        '--remove',
        nargs='+',
        metavar='KEY',
        help="Supprime des raccourcis")

    parser.add_argument(
        '-I',
        '--init',
        nargs='?',
        metavar='PATH',
        const='.',
        help="Initialise un fichier .svinfo dans le répertoire courant (ou le répertoire "+
             "PATH donné). Équivalent de la commande linux 'touch .svinfo'.")


    # FLAG OPTIONS:

    parser.add_argument(
        '-f',
        '--force',
        action='store_true',
        help="Forcer l'écrasement des raccourcis existants avec -a ou -A")

    parser.add_argument(
        '-u',
        '--absolute',
        action='store_true',
        help="Pour l'option --add et --list: utilise des chemins absolus")

    parser.add_argument(
        '-j',
        '--json',
        action='store_true',
        help="Pour les options -l, --get-keys: affiche les informations au format json")


    # OPTIONS THAT WILL BE HIDDEN IN THE SHORT HELP:

    if not short_help:

        # COMPLETION:

        parser.add_argument(
            '--get-completions',
            metavar='WORD',
            help="Affiche la liste des complétions possibles pour le début de mot indiqué")


        # SHELL SCRIPT OPTIONS:

        parser.add_argument(
            '--sh-dump',
            action="store_true",
            help="Affiche des commandes shell de génération d'aliases et de variables (argument interne)")

        parser.add_argument(
            '--sh',
            default='bash',
            choices=AVAILABLE_SHELLS,
            help="Définit le langage shell dans lequel générer le script")

        parser.add_argument(
            '--sh-conf',
            metavar='CONFFILE',
            help="Utilise un fichier de config pour remplacer --sh-vars-templ, --sh-aliases-templs")


        # SHELL SCRIPT OPTIONS: VARIABLE OPTIONS:

        parser.add_argument(
            '--sh-vars-templ',
            metavar='TEMPLATE',
            help="Définit le patron pour générer les noms de variable shell")

        parser.add_argument(
            '--sh-include-vars',
            metavar='VAL',
            help="Indique s'il faut inclure les variables shell")

        parser.add_argument(
            '--sh-vars-root-name',
            metavar='VARNAME',
            default=None,
            help="Spécifie un nom de variable à définir pour le chemin racine du projet")


        # SHELL SCRIPT OPTIONS: ALIAS OPTIONS:

        parser.add_argument(
            '--sh-aliases-templ',
            metavar='TEMPLATE',
            help="Définit le patron pour générer les noms d'alias shell")

        parser.add_argument(
            '--sh-aliases-root-name',
            metavar='ALIAS_NAME',
            help="Nom de l'alias à utiliser pour aller à la racine du projet courant")

        parser.add_argument(
            '--sh-include-aliases',
            metavar='VAL',
            help="Indique s'il faut inclure les alias shell")

        parser.add_argument(
            '--sh-include-cd',
            action='store_true',
            help="Ajoute au script shell une commande 'cd' pour l'alias donné")


    # HELP:

    parser.add_argument(
        '-h',
        '--help',
        action='store_true',
        help="Affiche l'aide")

    parser.add_argument(
        '-H',
        '--more-help',
        action='store_true',
        help="Affiche l'aide complète")

    parser.add_argument(
        '--version',
        action='store_true',
        help="Affiche la version")

    return parser




#-------------------- def show_extra_help()
def show_extra_help():
    # TODO
    print('''
''')


#-------------------- def show_version()
def show_version():
    print('pysv {}'.format(VERSION))



#-------------------- if __name__ == '__main__'
if __name__ == '__main__':

    try:
        parser = create_parser()
        args = parser.parse_args(sys.argv[1:])

        # Options -h and -H: Help

        if args.help:
            shorthelp_parser = create_parser(True)
            shorthelp_parser.print_help()
            sys.exit()

        if args.more_help:
            parser.print_help()
            show_extra_help()
            sys.exit()

        if args.version:
            show_version()
            sys.exit()

        # Option -C / --directory: Change directory

        if args.directory:
            os.chdir(args.directory)

        # Option -I / --init: Initialize a new svinfo project

        if args.init:
            init_path = args.init

            svinfo_file = Path(init_path) / '.svinfo'

            if svinfo_file.exists():
                if not args.force:
                    raise(PysvError(
                        "Impossible d'initialiser: le fichier existe; utilisez "+
                        "l'option -f pour forcer la suppression de l'ancien fichier."
                    ))
                svinfo_file.unlink()

            else:
                found_svinfo = Svinfo.findSvinfo(init_path)
                if found_svinfo != None and found_svinfo.exists() and not args.force:
                    raise(PysvError(
                        "Un fichier .svinfo a été trouvé en amont: {}\n".format(found_svinfo) +
                        "Utilisez l'option -f pour forcer une création imbriquée."
                    ))

            svinfo_file.touch()

            path = init_path == '.' and 'courant' or init_path
            print("Fichier .svinfo initialisé dans le dossier {}.".format(path))

            sys.exit()

        # Option -A / --add-absolute: Add the current directory as an absolute path

        if args.add_absolute:
            args.absolute = True
            args.add = args.add_absolute
        possibly_create_file = bool(args.add)

        # Option -g / --global: Use the global svinfo file (~/.svinfo.global)

        if args.is_global:
            standard_global_svinfo_path = Path(os.environ['HOME']) / GLOBAL_SVINFO_FILENAME
            svinfo_path = os.getenv('PYSV_GLOBAL_SVINFO', standard_global_svinfo_path)
        else:
            svinfo_path = args.svinfo_path
        svinfo = Svinfo(svinfo_path, possibly_create_file)


        if not svinfo.exists:
            raise(PysvError("Aucun projet trouvé"))


        # Option -p / --get-project-dir: Show the current base dir of the used svinfo file

        if args.get_project_dir :
            print(svinfo.project_dir)
            sys.exit()

        # Option -i / --get-svinfo-file: Show the used svinfo file

        if args.get_svinfo_file :
            print(svinfo.svinfo_file)
            sys.exit()

        # Option --check: Check the health of a svinfo file

        if args.check:
            format = 'svinfo'
            if args.get_keys:
                format = 'get_keys'
            svinfo.check(format=format)
            sys.exit()


        # Further options need no error about the used svinfo file:

        if svinfo.error:
            raise svinfo.error


        # Option -l / --list: List shortcuts

        if args.list != None:
            if args.list == []:
                print(svinfo.getDump(args.absolute, args.json))
            else:
                if args.json:
                    import json
                    paths = [ str(svinfo.getKey(key, args.absolute)) for key in args.list ]
                    print(json.dumps(paths))
                else:
                    for key in args.list:
                        print(svinfo.getKey(key, args.absolute))
            sys.exit()

        # Option --get-completions: Get completions

        if args.get_completions != None:
            print("\n".join(svinfo.getCompletions(args.get_completions)))
            sys.exit()

        # Option -a / -add / -A / --add-absolute: Add a shortcut

        if args.add:
            key, path = svinfo.do_add(args.add, args.absolute, args.force)
            svinfo.save()
            print("Raccourci '{} => {}' ajouté.".format(key, path))
            sys.exit()

        # Option -r / --remove: Remove shortcuts

        if args.remove:
            for key in args.remove:
                path = svinfo.getShortcutPath(key)
                if svinfo.remove(key):
                    svinfo.save()
                    print("Raccourci '{} => {}' retiré.".format(key, path))
            sys.exit()

        # Option -s / --search: Search a path in existing shortcuts

        if args.search:
            keys = svinfo.searchForPath(args.search)
            if not keys:
                raise(PysvError("Aucun raccourci trouvé pour ce dossier"))
            else:
                print(','.join(keys))
            sys.exit()

        # Option --get-keys: Show shortcut keys

        if args.get_keys:
            if args.json:
                import json
                print(json.dumps(list(svinfo.getKeys())))
            else:
                print('  '.join(svinfo.getKeys()))
            sys.exit()

        # Option --sh-dump: Dump shell script commands which define actual shortcuts
        # All shell script options are handled in the getShellScriptDump() method:

        if args.sh_dump:
            print(svinfo.getShellScriptDump(args))
            sys.exit(2)

        print(svinfo.getShortcutPath(args.keyword))

    except PysvError as e:
        print(str(e), file=sys.stderr)
        sys.exit(1)

