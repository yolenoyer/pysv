
function sv
	set -l config_dir ~/.pysv
	set -l tmp (mktemp) || return 1
	set -l options --sh=fish --sh-dump --sh-include-cd

	if [ "$PYSV_PARSE_CONF" = 1 ]
		set -l sh_conf
		if [ -f $config_dir/sv.conf ]
			set sh_conf $config_dir/sv.conf
		else
			set sh_conf /usr/local/lib/pysv/default_config/sv.conf
		end
		set options $options --sh-conf $sh_conf
	end

	pysv $options $argv >$tmp
	set -l ret $status

	switch $ret
		case 1
			rm $tmp  >/dev/null 2>&1
			return 1
		case 2
			source $tmp
		case '*'
			cat $tmp
	end

	rm $tmp  >/dev/null 2>&1
end

function sva
	if [ $argv[1] = "-h" ]
		echo "Usage: sva <KEY> [PATH]"
		echo "Raccourci pour sv -a: ajoute le raccourci de projet <KEY> pour le répertoire"
		echo "courant, ou pour [PATH] si précisé."
		return
	end

	sv -a $argv || return
	sv # TODO: à améliorer (double exécution)
end
