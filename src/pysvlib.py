# coding: utf-8

import os
from pathlib import Path
import re
import shlex




# Version:
VERSION = 'v0.2'

# Nom du fichier à vérifier à chaque niveau de répertoire:
SVINFO_FILENAME = Path('.svinfo')

# Nom du fichier svinfo global (à chercher dans le répertoire $HOME):
GLOBAL_SVINFO_FILENAME = '.svinfo.global';

# Noms de variables interdits lors de la génération des scripts shell:
FORBIDDEN_VARIABLE_NAMES = [
    '_'   , 'DISPLAY', 'HOME' , 'LANG', 'LOGNAME', 'MAIL'  , 'OLDPWD',
    'PATH', 'PWD'    , 'SHELL', 'TERM', 'USER'   , 'VISUAL',
]

AVAILABLE_SHELLS = [ 'bash', 'fish' ];




#-------------------- class PysvError(Exception)
class PysvError(Exception):
    pass


#-------------------- def filterByPrefix(arr, prefix)
def filterByPrefix(arr, prefix):
    return [ s for s in arr if s[:len(prefix)] == prefix ]


#-------------------- def isYes(s)
def isYes(s):
    return s in ['y', 'yes']




#-------------------- class NameTransformer
class NameTransformer:
    #-------------------- def upperFirstLetter(self, word)
    def upperFirstLetter(self, word):
        return word.capitalize()


    #-------------------- def upperAll(self, word)
    def upperAll(self, word):
        return word.upper()


    #-------------------- def noTransform(self, word)
    def noTransform(self, word):
        return word


    #-------------------- def __init__(self, template)
    def __init__(self, template):
        try:
            open_paren = template.index('(')
            close_paren = template.index(')')

            if open_paren > close_paren:
                raise(PysvError("parenthèses mal placées"))

            self.prefix = template[:open_paren]
            self.templ_name = template[open_paren+1:close_paren]
            self.suffix = template[close_paren+1:]

            if not re.fullmatch('[A-Za-z_]{2,}', self.templ_name):
                raise(PysvError("nom de patron entre parenthèses '{}'".format(self.templ_name)))

            if self.templ_name.isupper():
                self.case_transform = self.upperAll
            elif self.templ_name[0].isupper():
                self.case_transform = self.upperFirstLetter
            else:
                self.case_transform = self.noTransform

        except Exception as e:
            pysv_msg = isinstance(e, PysvError) and " ({})".format(e) or ''
            raise(PysvError(
                'Patron de nom de variable/alias shell invalide{}: "{}"'
                .format(pysv_msg, template)
            ))


    #-------------------- def transform(self, name)
    def transform(self, name):
        return self.prefix + self.case_transform(name) + self.suffix




#-------------------- class ShOptions
class ShOptions:
    #-------------------- def __init__(self, args)
    def __init__(self, args):
        self.include_vars = self.include_aliases = False

        # Templates par défaut, si on n'a ni option --sh-*-templ, ni fichier de config
        # --sh-conf:
        self.vars_templ    = '(name)'
        self.aliases_templ = ',(name)'
        self.root_name = None
        self.aliases_root_name = None
        self.lang = 'bash'

        if args.sh_conf:
            self.parseConf(args.sh_conf)

        if args.sh_include_vars:      self.include_vars      = isYes(args.sh_include_vars)
        if args.sh_include_aliases:   self.include_aliases   = isYes(args.sh_include_aliases)
        if args.sh_vars_templ:        self.vars_templ        = args.sh_vars_templ
        if args.sh_aliases_templ:     self.aliases_templ     = args.sh_aliases_templ
        if args.sh_aliases_root_name: self.aliases_root_name = args.sh_aliases_root_name
        if args.sh_vars_root_name:    self.root_name         = args.sh_vars_root_name
        if args.sh:                   self.lang              = args.sh

        self.include_cd = args.sh_include_cd

        if self.include_vars:
            self.transform_var = NameTransformer(self.vars_templ).transform
        if self.include_aliases:
            self.transform_alias = NameTransformer(self.aliases_templ).transform


    #-------------------- def parseConf(self, conf_file)
    def parseConf(self, conf_file):
        try:
            with open(conf_file, 'r') as infile:
                pattern = re.compile(r'(\w+)\s*=\s*(\S+)')
                for line in infile:
                    match = pattern.match(line)
                    if not match:
                        continue

                    varname, value = match.group(1, 2)

                    if   varname == 'ShellVarsEnable':      self.include_vars = isYes(value)
                    elif varname == 'ShellVarsTemplate':    self.vars_templ = value
                    elif varname == 'ShellVarsRootName':    self.root_name = value
                    elif varname == 'ShellAliasesEnable':   self.include_aliases = isYes(value)
                    elif varname == 'ShellAliasesTemplate': self.aliases_templ = value
                    elif varname == 'ShellAliasesRootName': self.aliases_root_name = value

        except FileNotFoundError as e:
            raise PysvError(
                "Impossible d'ouvrir le fichier de configuration '{}'"
                .format(conf_file)
            )




#-------------------- class ScriptGenerator
class ScriptGenerator:
    #-------------------- def __init__(self, lang)
    def __init__(self, lang):
        self.lang = lang

    #-------------------- def quote(self, value)
    def quote(self, value):
        return shlex.quote(str(value))

    #-------------------- def alias(self, name, value)
    def setAlias(self, name, value):
        return "alias {}={}".format(name, self.quote(value))

    #-------------------- def setVariable(self, name, value)
    def setVariable(self, name, value):
        if self.lang == 'bash':
            return "{}={}".format(name, self.quote(value))
        elif self.lang == 'fish':
            return "set {} {}".format(name, self.quote(value))

    #-------------------- def changeDirectory(self, directory)
    def changeDirectory(self, directory):
        return "cd {}".format(self.quote(directory))




#-------------------- class SviPath
class SviPath:
    #-------------------- def __init__(self, svinfo, path)
    def __init__(self, svinfo, path):
        self.svinfo = svinfo
        self.setPath(path)


    #-------------------- def setPath(self, path)
    def setPath(self, path):
        self.path = path
        slash_split = self.path.split('/')
        self.key = slash_split[0]
        self.subpath = None
        if len(slash_split) > 1:
            self.subpath = '/'.join(slash_split[1:])


    #-------------------- def getKeyPath(self, absolute=False)
    def getKeyPath(self, absolute=False):
        return self.svinfo.getKey(self.key, absolute)


    #-------------------- def getPath(self, absolute=False)
    def getPath(self, absolute=False):
        path = self.getKeyPath(absolute)
        if self.subpath != None:
            path = path / self.subpath
        return path


    #-------------------- def hasSubpath(self)
    def hasSubpath(self):
        return self.subpath != None


    #-------------------- def getSubpath(self)
    def getSubpath(self):
        return self.subpath == None and None or Path(self.subpath)


    #-------------------- def isDir(self)
    def isDir(self):
        return self.getPath(True).is_dir()


    #-------------------- def exists(self)
    def exists(self):
        return self.getPath(True).exists()


    #-------------------- def listDir(self)
    def listDir(self):
        return os.listdir(self.getPath(True))


    #-------------------- def getCompletions(self)
    def getCompletions(self):
        svinfo = self.svinfo
        completions = []

        try:
            if not self.hasSubpath():
                completions = filterByPrefix(svinfo.getKeys(), self.path)
            else:
                path_parts = self.path.split('/')
                last_part = path_parts[-1]
                base_path = '/'.join(path_parts[:-1])
                svi_base_path = SviPath(svinfo, base_path)
                files = filterByPrefix(svi_base_path.listDir(), last_part)
                completions = [ "{}/{}".format(base_path, f) for f in files ]

            completions = [ comp + (SviPath(svinfo, comp).isDir() and '/' or '') for comp in completions ]

            if len(completions) == 1:
                svi_path = SviPath(svinfo, completions[0])
                if svi_path.isDir():
                    files = svi_path.listDir()
                    files = [ svi_path.path + f for f in files ]
                    completions.extend(files)

        except Exception as e:
            pass

        return completions




#-------------------- class Svinfo
class Svinfo:

    #-------------------- def __init__(self, from_path, create_file_if_possible=False)
    def __init__(self, from_path, create_file_if_possible=False):
        self.svinfo_file = self.findSvinfo(from_path)
        self.exists = self.svinfo_file and self.svinfo_file.is_file()
        self.error = None

        if self.svinfo_file:
            if not self.exists and create_file_if_possible:
                self.svinfo_file.touch()
                self.exists = True
            self.project_dir = self.svinfo_file.parent

        if self.exists:
            try:
                self.parseSvinfo()
            except PysvError as e:
                self.error = e


    #-------------------- def findSvinfo(from_path=None)
    @staticmethod
    def findSvinfo(from_path=None):
        if from_path == None:
            folder = Path.cwd()
            while True:
                svinfo_file = folder / SVINFO_FILENAME
                if svinfo_file.is_file(): break
                if folder.parent == folder: return None
                folder = folder.parent

        else:
            path = Path(from_path).expanduser()

            if path.is_file() or (not path.exists() and path.parent.is_dir()):
                svinfo_file = path
            elif path.is_dir():
                svinfo_file = path / SVINFO_FILENAME
            else:
                raise(PysvError("Le chemin {} n'existe pas.".format(path)))

        name = svinfo_file.name
        return svinfo_file.parent.resolve() / name


    #-------------------- def parseSvinfo(self)
    def parseSvinfo(self):
        blank_line = re.compile(r'^\s*$')
        line_pattern = re.compile(r'^([a-zA-Zéèàêùïà_,][a-zA-Zéèàêùïà0-9_,]*)\s+(?:(\?)\s+)?(.+)$')
        self.defs = {}
        with open(str(self.svinfo_file), 'r') as infile:
            for line_nr, line in enumerate(infile, 1):
                if blank_line.match(line) != None:
                    continue
                match = line_pattern.match(line)
                if match == None:
                    raise PysvError("Erreur de parse du fichier '{}', ligne {}".format(self.svinfo_file, line_nr))

                groups = match.groups()

                keys = groups[0].split(',')
                for key in keys:
                    self.defs[key] = groups[2]


    #-------------------- def getKeys(self, filter_paths=None)
    def getKeys(self, filter_paths=None):
        if not filter_paths:
            return self.defs.keys()
        else:
            return [ k for k in self.defs if str(self.defs[k]) in filter_paths ]


    #-------------------- def getFullPaths(self)
    def getFullPaths(self):
        keys = self.getKeys()
        paths = [ self.getKey(k) for k in keys ]
        return paths


    #-------------------- def check(self, format='svinfo')
    def check(self, format='svinfo'):
        paths = self.getFullPaths()
        inexistent_paths = []
        for path in paths:
            if not path.exists():
                inexistent_paths.append(str(path))

        if format == 'svinfo':
            nb_shortcuts = len(inexistent_paths)
            if nb_shortcuts == 0:
                print("Aucun raccourci cassé n'a été trouvé dans le fichier '{}'.".format(self.svinfo_file))
            else:
                s = nb_shortcuts > 1 and 's' or ''
                print("Trouvé {} raccourci{s} cassé{s} dans le fichier '{}':".format(nb_shortcuts, self.svinfo_file, s=s))
                print(self.getDump(filter_paths=inexistent_paths, prefix='  '))
        elif format == 'get_keys':
            for k in self.getKeys(filter_paths=inexistent_paths):
                print(k)


    #-------------------- def getShortcutPath(self, keyword_path)
    def getShortcutPath(self, keyword_path):
        if keyword_path == '/' : return self.project_dir
        if keyword_path == ''  : raise PysvError("Mot-clé attendu.")

        svi_path = SviPath(self,  keyword_path)
        return svi_path.getPath(True)


    #-------------------- def relPathTo(self, path)
    def relPathTo(self, path):
        return os.path.relpath(path, self.project_dir)


    #-------------------- def getShortcutsByPath(self, filter_paths=None)
    def getShortcutsByPath(self, filter_paths=None):
        paths = {}
        for key, path in self.defs.items():
            if filter_paths and path not in filter_paths:
                continue
            if path not in paths:
                paths[path] = []
            paths[path].append(key)
        return paths


    #-------------------- def getKey(self, key, absolute=False)
    def getKey(self, key, absolute=False):
        if key == '':
            path = '.'
        elif key not in self.defs:
            raise PysvError("Le mot-clé '{}' n'existe pas.".format(key))
        else:
            path = self.defs[key]

        if absolute:
            return Path(self.project_dir) / path
        else:
            return Path(path)


    #-------------------- def searchForPath(self, path)
    def searchForPath(self, path):
        path = Path(path).resolve()
        shorcuts_by_path = self.getShortcutsByPath()

        for def_path, shortcuts in shorcuts_by_path.items():
            def_path = self.project_dir / def_path
            if path == def_path:
                return shortcuts

        return []


    #-------------------- def add(self, keyword, path, absolute=False)
    def add(self, keyword, path, absolute=False):
        self.defs[keyword] = absolute and path.resolve() or self.relPathTo(path)
        return keyword, self.defs[keyword]


    #-------------------- def do_add(self, add_args, absolute=False, force=False)
    def do_add(self, add_args, absolute=False, force=False):
        key = add_args[0]

        if key in self.defs and not force:
            raise PysvError("Le mot-clé '{}' existe déjà ({}). Utilisez -f pour forcer."
                    .format(key, self.defs[key]))

        if len(add_args) > 1:
            path = Path(add_args[1])
        else:
            path = Path.cwd()

        return self.add(key, path, absolute)


    #-------------------- def remove(self, keyword)
    def remove(self, keyword):
        has_keyword = keyword in self.defs
        if has_keyword:
            del self.defs[keyword]
        return has_keyword


    #-------------------- def getCompletions(self, arg)
    def getCompletions(self, arg):
        p = SviPath(self, arg)
        return p.getCompletions()


    #-------------------- def getShellScriptDump(self, args)
    def getShellScriptDump(self, args):
        # Détermine les options pour le dump, en prenant en compte les arguments fournis
        # en ligne de commande, et éventuellement par un fichier de configuration:
        opts = ShOptions(args)
        generator = ScriptGenerator(opts.lang)

        script = []
        for key, shortcut_path in self.defs.items():
            # Si shortcut_path est absolu, alors self.project_dir sera ignoré, ce qui
            # permet l'utilisation des raccourcis globaux dans les fichiers .svinfo:
            path = self.project_dir / shortcut_path

            if opts.include_aliases and path.is_dir():
                cd_command = generator.changeDirectory(path)
                script.append(generator.setAlias(opts.transform_alias(key), cd_command))

            if opts.include_vars:
                var_name = opts.transform_var(key)
                if not var_name in FORBIDDEN_VARIABLE_NAMES:
                    script.append(generator.setVariable(var_name, path))

        if opts.include_aliases and opts.aliases_root_name:
            cd_command = generator.changeDirectory(self.project_dir)
            script.append(generator.setAlias(opts.aliases_root_name, cd_command))

        if opts.include_vars:
            quoted_path = shlex.quote(str(self.project_dir))
            if opts.root_name != None:
                script.append(generator.setVariable(opts.root_name, self.project_dir))

        if opts.include_cd and args.keyword:
            script.append(generator.changeDirectory(self.getShortcutPath(args.keyword)))

        return "\n".join(script)


    #-------------------- def getJsonDump(self, filter_paths=None)
    def getJsonDump(self, filter_paths=None):
        # TODO: filter_paths

        import json

        data = { item[0] : str(self.project_dir / item[1]) for item in self.defs.items() }
        data['root'] = str(self.project_dir)

        return json.dumps(data);


    #-------------------- def getDump(self, absolute=False, json=False, ....)
    def getDump(self, absolute=False, json=False, filter_paths=None, prefix=''):
        # TODO: gérer absolute
        if json:
            return self.getJsonDump(filter_paths=filter_paths)

        shorcuts_by_path = self.getShortcutsByPath(filter_paths=filter_paths)

        dump = ''

        margin = 2  # Marge minimum pour l'alignement de la 1ère colonne

        # Pour l'alignement:
        max_col1_width = 0
        for shortcuts in shorcuts_by_path.values():
            shortcuts_strlen = len(','.join(shortcuts))
            if max_col1_width < shortcuts_strlen:
                max_col1_width = shortcuts_strlen

        for path, shortcuts in shorcuts_by_path.items():
            if len(dump) != 0:
                dump += '\n'
            dump += prefix
            shortcuts_str = ','.join(shortcuts)
            dump += shortcuts_str
            # Pour l'alignement:
            nb_spaces = max_col1_width - len(shortcuts_str) + margin
            dump += nb_spaces * ' '
            dump += str(path)

        return dump


    #-------------------- def save(self)
    def save(self):
        with open(str(self.svinfo_file),'w') as f:
            f.write(self.getDump() + '\n')
